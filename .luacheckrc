globals = {
    "money3",
    "money",
}

read_globals = {
    canonical_name = {fields = {"get"}},
    "minetest",
    lurkcoin = {fields = {"change_bank"}},
    unified_money = {fields = {"register_backend"}},
    string = {fields = {"split", "trim"}},
    table = {fields = {"copy", "indexof"}}
}

files["lockedsign.lua"].ignore = {""}
